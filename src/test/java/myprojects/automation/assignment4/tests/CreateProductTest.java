package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {
    private ProductData product;

    @Test(dataProvider = "authorization")
    public void createNewProduct(String login, String password) {
        actions.login(login, password);
        this.product = ProductData.generate();
        actions.createProduct(product);
    }

    @Test(dependsOnMethods = {"createNewProduct"})
    public void checkTheProduct(){
        actions.goToTheStore();
        actions.checkTheNameOfTheProduct(this.product);
        actions.detailedInfoAboutProduct(this.product);
        actions.checkAllFields(this.product);
    }
}
