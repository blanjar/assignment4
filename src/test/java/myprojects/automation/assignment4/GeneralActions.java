package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    private By loginButtonName = By.name("submitLogin");
    private By name = By.id("form_step1_name_1");
    private By createNewProduct = By.id("page-header-desc-configuration-add");
    private By quantityButton = By.id("tab_step3");
    private By quantityInput = By.id("form_step3_qty_0");
    private By priceButton = By.id("tab_step2");
    private By priceInput = By.id("form_step2_price_ttc");
    private By catalogueMenu = By.id("subtab-AdminCatalog");
    private By catalogueProducts = By.id("subtab-AdminProducts");
    private By allProducts = By.xpath("//*[@id=\"content\"]/section/a");
    private By productInformation = By.className("product-information");

    private By emailInput = By.id("email");
    private By passwordInput = By.id("passwd");
    private By mainId = By.id("main");
    private By mainDiv = By.id("main-div");
    private By contentId = By.id("content");
    private By productClass = By.className("products");
    private By nextLink = By.xpath("//a[@rel='next']");
    private By productNameElement = By.cssSelector("h1.h1");
    private By priceElement = By.cssSelector("span[content]");
    private By qtyElement = By.cssSelector(".product-quantities");

    private By productActivateBtn = By.className("switch-input");
    private By popupConfirmationMsg = By.className("growl-close");
    private By productSaveBtn = By.cssSelector("button.js-btn-save");
    private By productSaveBtn2 = By.cssSelector("input.save");


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        CustomReporter.log("Login as user - " + login);

        driver.navigate().to(Properties.getBaseAdminUrl());
        waitForContentLoad(loginButtonName);
        driver.findElement(this.emailInput).sendKeys(login);
        driver.findElement(this.passwordInput).sendKeys(password);
        driver.findElement(this.loginButtonName).click();
        waitForContentLoad(this.mainId);
    }

    public void createProduct(ProductData newProduct) {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(this.catalogueMenu)).build().perform();
        waitForContentLoad(this.catalogueProducts);
        actions.moveToElement(driver.findElement(catalogueProducts)).click().build().perform();
        waitForContentLoad(this.mainDiv);

        driver.findElement(this.createNewProduct).click();
        waitForContentLoad(name);

        WebElement name = driver.findElement(this.name);
        name.sendKeys(newProduct.getName());

        driver.findElement(this.quantityButton).click();

        WebElement qty = driver.findElement(this.quantityInput);
        qty.sendKeys(newProduct.getQty().toString());

        driver.findElement(this.priceButton).click();

        WebElement price = driver.findElement(this.priceInput);
        price.sendKeys(newProduct.getPrice());

        driver.findElement(this.productActivateBtn).click();

        checkNotificationPopup(this.popupConfirmationMsg);

        if (driver.findElement(this.productSaveBtn).isDisplayed()) {
            driver.findElement(this.productSaveBtn).click();
        } else {
            driver.findElement(this.productSaveBtn2).click();
        }

        checkNotificationPopup(this.popupConfirmationMsg);
    }

    public void goToTheStore() {
        driver.navigate().to(Properties.getBaseUrl());
        waitForContentLoad(this.contentId);
        driver.findElement(this.allProducts).click();
        waitForContentLoad(this.productClass);
        driver.findElement(this.nextLink).click();
        waitForContentLoad(this.productClass);
    }

    public void checkTheNameOfTheProduct(ProductData newProduct) {
        Assert.assertTrue(driver.findElement(By.xpath(".//*[text()='" + newProduct.getName() + "']"))
                .getText().contains(newProduct.getName()), "Name of the products must match");
    }

    public void detailedInfoAboutProduct(ProductData newProduct){
        driver.findElement(By.xpath(".//*[text()='" + newProduct.getName() + "']")).click();
        waitForContentLoad(productInformation);
    }

    public void checkAllFields(ProductData newProduct){
        Assert.assertTrue(driver.findElement(this.productNameElement)
                .getText().contains(newProduct.getName().toUpperCase()),"Name of the product is correct");
        Assert.assertTrue(driver.findElement(this.priceElement)
                .getText().contains(newProduct.getPrice()), "Price is correct");
        Assert.assertTrue(driver.findElement(this.qtyElement)
                .getText().contains(newProduct.getQty().toString()), "Quantity is correct");
    }

    /**
     * Checks notification popup visibility and try to close it
     */
    public void checkNotificationPopup(By by) {
        this.waitForContentLoad(by);
        if (driver.findElement(by).isDisplayed()) {
            try {
                driver.findElement(by).click();
            } catch (Exception ex) {
                // Sometimes popup is showed but appeared error like following:
                // org.openqa.selenium.StaleElementReferenceException: stale element reference: element is not attached to the page document
                // just ignore it when close popup
                System.out.println("Popup is present but not attached to DOM.");
            }
        }
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad(By element) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}
